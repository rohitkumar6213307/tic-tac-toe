import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsPage extends StatefulWidget {
  const SettingsPage({super.key});

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  late SharedPreferences sp;
  late List<Map<String, dynamic>> list = [];

  @override
  void initState() {
    super.initState();
    _loadScore();
  }

  void _loadScore() async {
    sp = await SharedPreferences.getInstance();
    List<String> scores = sp.getStringList("SCORES") ?? [];

    setState(() {
      for (var element in scores) {
        list.add(jsonDecode(element));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.all(12),
          decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/bg.jpeg"), fit: BoxFit.cover)),
          child: Column(
            children: [
              // header
              buildHeader(context),
              const SizedBox(height: 20),

              // list of scores
              buildRow("Player1 (O)", "Player2 (X)", showBorder: true),
              scoresList()
            ],
          ),
        ),
      ),
    );
  }

  Expanded scoresList() {
    return Expanded(
      child: ListView(
        children: List.from(
          list.map(
            (element) =>
                buildRow(element["O"].toString(), element["X"].toString()),
          ),
        ),
      ),
    );
  }

  Row buildHeader(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        // back button
        InkWell(
          child: Image(
              image: const AssetImage("assets/back.png"),
              height: MediaQuery.of(context).size.height * 0.035),
          onTap: () => Navigator.pop(context),
        ),
        const SizedBox(width: 20),
        // title
        Text(
          "Leaderboard",
          style: TextStyle(
            fontSize: size.height * 0.035,
            fontWeight: FontWeight.bold,
            color: Colors.blueAccent,
          ),
        )
      ],
    );
  }

  Widget buildRow(String label1, String label2, {bool showBorder = false}) {
    Size size = MediaQuery.of(context).size;
    TextStyle textStyle =
        TextStyle(fontSize: size.height * 0.03, color: Colors.blueAccent);
    return Container(
      decoration: BoxDecoration(
        border: showBorder
            ? const Border(
                bottom: BorderSide(color: Colors.blueAccent, width: 2),
                top: BorderSide(color: Colors.blueAccent, width: 2),
              )
            : null,
      ),
      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 4),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Text(label1, style: textStyle),
          const SizedBox(width: 20),
          Text(label2, style: textStyle)
        ],
      ),
    );
  }
}
