import 'dart:collection';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PlaygroundPage extends StatefulWidget {
  const PlaygroundPage({super.key});

  @override
  State<PlaygroundPage> createState() => _PlaygroundPageState();
}

class _PlaygroundPageState extends State<PlaygroundPage> {
  // states
  List<String> list = ['', '', '', '', '', '', '', '', ''];
  bool myTurn = true;
  Map<String, String> hm = HashMap();
  String winner = "";
  List<int> winningIndex = List.empty();
  late SharedPreferences sp;
  bool newGame = true;
  int filled = 0;

  @override
  void initState() {
    super.initState();
    _initializeSP();

    hm["X"] = "0";
    hm["O"] = "0";
  }

  void _initializeSP() async {
    sp = await SharedPreferences.getInstance();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/bg.jpeg"), fit: BoxFit.cover),
          ),
          width: size.width,
          height: size.height,
          padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
          child: Column(
            children: [
              const SizedBox(height: 12),

              // header
              buildHeader(context),

              const SizedBox(height: 52),

              // grid
              buildGrid(),

              // reset button
              resetButton(),

              const SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }

  void resetState() {
    setState(() {
      winner = "";
      winningIndex = [];
      list = ['', '', '', '', '', '', '', '', ''];
      myTurn = true;
      filled = 0;
    });
  }

  InkWell resetButton() {
    return InkWell(
      onTap: () {
        resetState();
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: Colors.blueAccent,
        ),
        padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 12),
        child: Text(
          "Reset",
          style: TextStyle(
              fontSize: MediaQuery.of(context).size.height * 0.025,
              fontWeight: FontWeight.bold,
              color: Colors.white),
        ),
      ),
    );
  }

  Widget buildGrid() {
    return Flexible(
      child: GridView.builder(
        itemCount: 9,
        gridDelegate:
            const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
        itemBuilder: (context, index) => box(index),
      ),
    );
  }

  Row buildHeader(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        buildBackButton(context),
        buildScoreBoard(context),
        buildSettingsButton(context),
      ],
    );
  }

  Widget buildBackButton(BuildContext context) {
    return InkWell(
      child: Image(
        image: const AssetImage("assets/back.png"),
        height: MediaQuery.of(context).size.height * 0.05,
      ),
      onTap: () => Navigator.pop(context),
    );
  }

  Widget buildPlayerInfo(BuildContext context, String playerName,
      String assetImage, bool isCurrentPlayer) {
    return Column(
      children: [
        Opacity(
          opacity: isCurrentPlayer ? 1 : 0.25,
          child: Image(
            image: AssetImage(assetImage),
            height: MediaQuery.of(context).size.height * 0.05,
          ),
        ),
        Text(
          playerName,
          style: TextStyle(
            color: Colors.white,
            fontSize: MediaQuery.of(context).size.height * 0.02,
          ),
        ),
      ],
    );
  }

  Widget buildScoreBoard(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        buildPlayerInfo(context, "Player 1", "assets/zero.png", myTurn),
        const SizedBox(width: 12),
        Text(
          "${hm['O']}:${hm['X']}",
          style: TextStyle(
            fontSize: MediaQuery.of(context).size.height * 0.045,
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(width: 12),
        buildPlayerInfo(context, "Player 2", "assets/cross.png", !myTurn),
      ],
    );
  }

  Widget buildSettingsButton(BuildContext context) {
    return InkWell(
      child: Image(
        image: const AssetImage("assets/settings.png"),
        height: MediaQuery.of(context).size.height * 0.05,
      ),
      onTap: () => Navigator.pushNamed(context, "/settings"),
    );
  }

  Widget box(idx) {
    return InkWell(
      onTap: () {
        if (list[idx].isNotEmpty) return;
        setState(() {
          if (myTurn) {
            list[idx] = "O";
          } else {
            list[idx] = "X";
          }
          myTurn = !myTurn;
          filled++;
        });

        if (filled >= 3) {
          _checkWinner();
        }
      },
      child: Container(
        height: 20,
        decoration: winningIndex.contains(idx)
            ? BoxDecoration(
                border: Border.all(color: Colors.redAccent, width: 2),
              )
            : BoxDecoration(
                border: Border.all(color: Colors.blueAccent, width: 1),
              ),
        child: Center(
            child: list[idx] == "O"
                ? Image(
                    image: const AssetImage("assets/zero.png"),
                    height: MediaQuery.of(context).size.width * 0.1)
                : list[idx] == "X"
                    ? Image(
                        image: const AssetImage("assets/cross.png"),
                        height: MediaQuery.of(context).size.width * 0.1,
                      )
                    : null),
      ),
    );
  }

  Future<void> _dialogBuilder(BuildContext context, String text) {
    Size size = MediaQuery.of(context).size;
    return showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            "Result",
            style: TextStyle(
                fontSize: size.height * 0.05,
                fontWeight: FontWeight.w500,
                color: Colors.blueAccent),
            textAlign: TextAlign.center,
          ),
          content: SizedBox(
            width: size.width * 0.65,
            child: Text(
              text,
              style: TextStyle(fontSize: size.height * 0.025),
              textAlign: TextAlign.center,
            ),
          ),
        );
      },
    ).then((value) {
      if (value == null) {
        resetState();
      }
    });
  }

  void _checkWinner() {
    // Define winning combinations (rows, columns, diagonals)
    final List<List<int>> winningCombinations = [
      [0, 1, 2], [3, 4, 5], [6, 7, 8], // Rows
      [0, 3, 6], [1, 4, 7], [2, 5, 8], // Columns
      [0, 4, 8], [2, 4, 6] // Diagonals
    ];

    // Check each winning combination
    for (final combination in winningCombinations) {
      if (list[combination[0]] == list[combination[1]] &&
          list[combination[0]] == list[combination[2]] &&
          list[combination[0]].isNotEmpty) {
        setState(() {
          winner = list[combination[0]];
          winningIndex = combination;
        });

        // Update winner count
        hm.update(winner, (value) => (int.parse(value) + 1).toString());

        // Show message if a winner is found
        _showMessage(winner == "O" ? "Player 1 WON!" : "Player 2 WON!");
        return;
      }
    }

    // Check for a draw
    if (filled == 9 && winner.isEmpty) {
      _showMessage("Game Draw");
    }
  }

  void _showMessage(String message) {
    _dialogBuilder(context, message);

    // fetch scores from shared preferences
    List<String> scores = sp.getStringList("SCORES") ?? [];

    if (newGame) {
      scores.add(json.encode(hm));
    } else {
      scores[scores.length - 1] = json.encode(hm);
    }
    sp.setStringList("SCORES", scores);

    newGame = false;
  }
}
