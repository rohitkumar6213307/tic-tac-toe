import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: size.width,
          decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/bg.jpeg"), fit: BoxFit.cover)),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text(
                  "TIC TAC TOE",
                  style: TextStyle(
                      fontSize: size.height * 0.05,
                      fontWeight: FontWeight.bold,
                      color: Colors.blueAccent),
                ),
                Image(
                  image: const AssetImage("assets/home.png"),
                  height: size.height * 0.25,
                ),
                InkWell(
                  onTap: () {
                    Navigator.pushNamed(context, '/play');
                  },
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12),
                      color: Colors.blueAccent,
                    ),
                    padding: const EdgeInsets.symmetric(
                        horizontal: 24, vertical: 12),
                    child: Text(
                      "Play!",
                      style: TextStyle(
                          fontSize: size.height * 0.0275,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                  ),
                )
              ]),
        ),
      ),
    );
  }
}
